export default ({
    execute,
    spawn
}) => {
    const
        _ = data => {
            if(data.startsWith('ERROR: JAVA_HOME is not set'))
                throw new Error('MISSING_JAVA_DEPENDENCY');
            return data;
        },
        /**
         * @returns {Promise<{
         *   lastSeenTimestamp: number,
         *   isCurrent: boolean,
         *   createdDate: string,
         *   createdTimestamp: number,
         *   lastSeenDate: string,
         *   name: string|null,
         *   id: number
         * }[]>}
         */
        listDevices = async () => ([
            ..._(await execute(['listDevices']))
                .matchAll(/- Device ([0-9]+)(\s\(this device\))?:\n\s{2}Name: (.+)\n\s{2}Created: ([0-9]+)\s\((.+)\)\n\s{2}Last seen: ([0-9]+)\s\((.+)\)/g)
        ]).map(([,
            id,
            isCurrent,
            name,
            createdTimestamp,
            createdDate,
            lastSeenTimestamp,
            lastSeenDate
        ]) => ({
            id: parseInt(id),
            isCurrent: !!isCurrent,
            name: name === 'null' ? null : name,
            createdTimestamp: parseInt(createdTimestamp),
            createdDate,
            lastSeenTimestamp: parseInt(lastSeenTimestamp),
            lastSeenDate
        })),
        /**
         * @param {number} id
         * @returns {Promise<void>}
         */
        removeDevice = async ({ id }) => {
            if(_(await execute(['removeDevice', '-d', id])).split('\n').includes('Error while removing device: [401] Authorization failed!'))
                throw new Error('401');
        };
    return {
        /** @returns {Promise<string>} */
        getVersion: async () => _(await execute(['--version'])).slice(11),
        /**
         * @param {string} [deviceName]
         * @param {function} [callback]
         * @returns {Promise<void>}
         */
        link: ({
            deviceName,
            callback = () => {}
        } = {}) => new Promise((resolve, reject) => {
            spawn({
                args: ['link', ...deviceName ? ['--name', deviceName] : []],
                onData: data => {
                    _(data);
                    let match;
                    if(data === 'Link request error: Connection closed!')
                        reject(new Error('LINK_REQUEST_TIMED_OUT'));
                    if(/^sgnl:\/\/linkdevice\?uuid=.+&pub_key=.+/.test(data))
                        callback({ linkUri: data });
                    if((match = data.match(/^INFO ProvisioningManager - Received link information from (\+[0-9]+), linking in progress \.\.\.$/)))
                        callback({ phoneNumber: match[1] });
                    if(/^Associated with: \+[0-9]+$/.test(data))
                        resolve();
                }
            });
        }),
        listDevices,
        removeDevice,
        /**
         * @todo Implement
         * @returns {Promise<void>}
         */
        unlink: async () => await removeDevice((await listDevices()).find(device => device.isCurrent))
    };
};